/**
 * Untuk menampilkan halaman login dan untuk menangani proses login
 */
package id.ac.unsyiah.jte.mobile;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet 
{
	/**
	 * Tangani HTTP GET
	 * Tampilkan halaman login
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException 
	{
        // Informasikan ke browser bahwa akan dikirim berkas html
		resp.setContentType("text/html");
        // Minta untuk bangkitkan HTML berdasarkan cetakan login.jsp
		RequestDispatcher jsp = req.getRequestDispatcher("login.jsp");
        // Bangkitkan HTML-nya
		jsp.forward(req, resp);		
	}
	
	/**
	 * Tangani HTTP POST
	 * Ambil informasi login, verifikasi, dan simpan ke session bahwa si user 
	 * telah login.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException
	{
		// Ambil informasi login yang dimasukkan pada form
		String username = req.getParameter("txtUsername");
		//String password = req.getParameter("passUsername");
		
		// Jika ada validasi benar username/password, tempatkan disini
		
    	// Ambil session yang ada. Jika belum ada, buat!
		HttpSession session = req.getSession(true);
		// Simpan informasi username untuk menandakan bahwa si user telah login
		session.setAttribute("username", username);
		// Pastikan lama tidak aktif sebelum dipaksa logout
		session.setMaxInactiveInterval(60*60);
		
        // Kirim ke halaman yang dilindungi
		resp.sendRedirect("/lindung");
	}
}

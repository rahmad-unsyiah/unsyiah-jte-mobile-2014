/**
 * Untuk menangani proses logout
 */
package id.ac.unsyiah.jte.mobile;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class LogoutServlet extends HttpServlet 
{
	/**
	 * Tangani HTTP GET
	 * Logout si user: hapus semua session object dan session-nya juga
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException 
	{
    	// Ambil session yang ada. 
		HttpSession session = req.getSession(true);
    	// Minta untuk menghapus sessionnya
		session.invalidate();

		// Kirim ke halaman login
		resp.sendRedirect("/login");
	}
}

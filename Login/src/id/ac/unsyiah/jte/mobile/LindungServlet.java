/**
 * Halaman yang dilindungi
 * User harus login terlebih dahulu sebelum bisa melihat halaman ini. Jika 
 * belum login akan dikirim ke halaman login
 */
package id.ac.unsyiah.jte.mobile;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class LindungServlet extends HttpServlet 
{
	/**
	 * Tangani HTTP GET
	 * Periksa apakah si user sudah login jika belum kirim ke halaman login
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException 
	{
		// Periksa apakah sudah ada session jika belum berarti belum login
		HttpSession session = req.getSession(false);
		if (session == null)
		{
			// Belum login, jadi kirim ke halaman login
			resp.sendRedirect("/login");
			return;
		}
		
		// Periksa apakah ada session object username, jika belum berarti 
		// belum login
		String username = (String) session.getAttribute("username");
		if (username == null)
		{
			// Belum login, jadi kirim ke halaman login
			resp.sendRedirect("/login");
			return;
		}			
		
        // Informasikan ke browser bahwa akan dikirim berkas html
		resp.setContentType("text/html");
        // Minta untuk bangkitkan HTML berdasarkan cetakan lindung.jsp
		RequestDispatcher jsp = req.getRequestDispatcher("lindung.jsp");
        // Bangkitkan HTML-nya
		jsp.forward(req, resp);		
	}
}

<!doctype html>
<html lang="id">
    <head>
        <title>Hasil</title>
    </head>
    <body>
        <h1>Hasil Pemilihan</h1>
        <h4>Caleg</h4>
        <table border="1">
            <thead>
                <tr>
                    <th>Caleg</th>
                    <th>Suara</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1. H. Guna Guna</td>
                    <td>${cacah1}</td>
                </tr>
                <tr>
                    <td>2. Ir. T. A. K. Berguna</td>
                    <td>${cacah2}</td>
                </tr>
                <tr>
                    <td>3. Hj. M. A. U. Berguna</td>
                    <td>${cacah3}</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>

/**
 * Tampilkan halaman pilihan yang ada
 * Sekalian juga inisialisasi entitas data cacah yang sesuai jika belum ada
 */
package id.ac.unsyiah.jte.mobile;

import java.io.IOException;

import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

@SuppressWarnings("serial")
public class PilihanServlet extends HttpServlet 
{
	/**
	 * Tangani HTTP GET
	 * Tampilkan halaman daftar pilihan dan pastikan entitas data cacah telah 
	 * terinisialisasi
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException 
	{
		// Inisialisasi entitas data cacah jika perlu
		inisialisasiDataCacah();
		
        // Informasikan ke browser bahwa akan dikirim berkas html
		resp.setContentType("text/html");
        // Minta untuk bangkitkan HTML berdasarkan cetakan pilihan.jsp
		RequestDispatcher jsp = req.getRequestDispatcher("pilihan.jsp");
        // Bangkitkan HTML-nya
		jsp.forward(req, resp);		
	}

	/**
	 * Tangani HTTP POST
	 * 
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException
	{
		// Inisialisasi entitas data cacah jika perlu
		inisialisasiDataCacah();
		
		// Ambil pilihan si user
		String pilihanStr = req.getParameter("caleg");
		// Ambil noKTP si User, sementara diabaikan
		// String noKTP = req.getParameter("noKTP");
		
		// Bangun query untuk mengambil data cacah saat ini untuk pilihan si 
		// user 
		Query query = new Query("Pilihan");
		// Kita filter untuk hanya mengambil entitas yang si user pilih 
		query.setFilter(new Query.FilterPredicate("noUrut",
												  Query.FilterOperator.EQUAL,
												  pilihanStr));
		// Ambil akses ke datastore
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		// Kirim query ke datastore
		PreparedQuery preparedQuery = datastoreService.prepare(query);
		// Jalankan query-nya dan ambil hasilnya
		Entity entity = preparedQuery.asSingleEntity();
		// Ambil property cacah, yang menyimpan data cacahan para pilihan
		Long cacah = (Long) entity.getProperty("cacah");
		// Increment data cacahnya
		cacah++;
		// Ubah property cacah dengan nilai cacah yang baru
		entity.setProperty("cacah", cacah);
		// Simpan ke datastore
		datastoreService.put(entity);
		
		// Kirim ke halaman hasil
		resp.sendRedirect("/hasil");
	}
	
	/**
	 * Untuk inisialisasi data cacah
	 * Jika data cacah belum ada akan dibuatkan entitas yang sesuai dengan 
	 * struktur sebagai berikut:
	 * 
	 *    Entity: Pilihan
	 *    Property:
	 *    	+ noUrut: Urutan pilihan
	 *    	+ cacah : Cacah untuk urutan itu 
	 *    
	 *    +--------+-------+
	 *    | noUrut | cacah |
	 *    +--------+-------+
	 *    |    1   |   5   |
	 *    |    2   |   6   |
	 *    |    3   |   3   |
	 *    +----------------+
	 */
	private void inisialisasiDataCacah()
	{
		// Ambil akses ke datastore
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		// Buat query baru ke entitas Pilihan untuk mengambil semua isi entitas
		Query query = new Query("Pilihan");
		// Kirim query ke datastore
		PreparedQuery preparedQuery = datastoreService.prepare(query);
		// Tentukan ambil data dari posisi mana dan sebanyak apa 
		FetchOptions fetchOptions = FetchOptions.Builder.withOffset(0);
		// Jalankan query-nya dan ambil hasilnya, berupa list karena query kita
		// meminta semua entitas
		List<Entity> daftarPilihan = preparedQuery.asList(fetchOptions);
		// Jika jumlah entitas != 3 berarti ada yang nggak beres, hapus semua
		if (daftarPilihan.size() > 0 && daftarPilihan.size() != 3)
			for (Entity satuPilihan : daftarPilihan)
				datastoreService.delete(satuPilihan.getKey());
		
		// Jika jumlah entitas 3 berarti sudah ada, jika belum isikan
		if (daftarPilihan.size() != 3)
		{
			// Urut 1
			Entity urut1 = new Entity("Pilihan");
			urut1.setProperty("noUrut", "1");
			urut1.setProperty("cacah", 0);		
			datastoreService = DatastoreServiceFactory.getDatastoreService();
			datastoreService.put(urut1);
			
			// Urut 2
			Entity urut2 = new Entity("Pilihan");
			urut2.setProperty("noUrut", "2");
			urut2.setProperty("cacah", 0);		
			datastoreService = DatastoreServiceFactory.getDatastoreService();
			datastoreService.put(urut2);

			// Urut 1
			Entity urut3 = new Entity("Pilihan");
			urut3.setProperty("noUrut", "3");
			urut3.setProperty("cacah", 0);		
			datastoreService = DatastoreServiceFactory.getDatastoreService();
			datastoreService.put(urut3);
		}
	}
}

/**
 * Menampilkan halaman hasil yang berisikan cacah pilihan per pilihan
 */
package id.ac.unsyiah.jte.mobile;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

@SuppressWarnings("serial")
public class HasilServlet extends HttpServlet 
{
	/**
	 * Tangani HTTP GET
	 * Tampilkan halaman hasil cacahan
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException 
	{
		// Ambil akses ke datastore
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		// Buat query baru ke entitas Pilihan
		Query query = new Query("Pilihan");

		// Buat filter untuk mengambil hasil cacah pilihan 1
		query.setFilter(new Query.FilterPredicate("noUrut",
				  								  Query.FilterOperator.EQUAL,
				  								  "1"));
		// Kirim query ke datastore
		PreparedQuery preparedQuery = datastoreService.prepare(query);
		// Jalankan query-nya dan ambil hasilnya
		Entity entity = preparedQuery.asSingleEntity();
		// Ambil property yang menampung hasil cacah
		long cacah1 = (long) entity.getProperty("cacah");
		// Kirim ke halaman untuk ditampilkan
		req.setAttribute("cacah1", cacah1);
		
		// Buat filter untuk mengambil hasil cacah pilihan 2
		query.setFilter(new Query.FilterPredicate("noUrut",
				  								  Query.FilterOperator.EQUAL,
				  								  "2"));
		// Kirim query ke datastore
		preparedQuery = datastoreService.prepare(query);
		// Jalankan query-nya dan ambil hasilnya
		entity = preparedQuery.asSingleEntity();
		// Jalankan query-nya dan ambil hasilnya
		long cacah2 = (long) entity.getProperty("cacah");
		// Kirim ke halaman untuk ditampilkan
		req.setAttribute("cacah2", cacah2);
		
		// Buat filter untuk mengambil hasil cacah pilihan 3
		query.setFilter(new Query.FilterPredicate("noUrut",
				  								  Query.FilterOperator.EQUAL,
				  								  "3"));
		// Kirim query ke datastore
		preparedQuery = datastoreService.prepare(query);
		// Jalankan query-nya dan ambil hasilnya
		entity = preparedQuery.asSingleEntity();
		// Jalankan query-nya dan ambil hasilnya
		long cacah3 = (long) entity.getProperty("cacah");
		// Kirim ke halaman untuk ditampilkan
		req.setAttribute("cacah3", cacah3);

        // Informasikan ke browser bahwa akan dikirim berkas html
		resp.setContentType("text/html");
        // Minta untuk bangkitkan HTML berdasarkan cetakan hasil.jsp
		RequestDispatcher jsp = req.getRequestDispatcher("hasil.jsp");
        // Bangkitkan HTML-nya
		jsp.forward(req, resp);		
	}
}

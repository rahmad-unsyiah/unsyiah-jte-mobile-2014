/**
 * Menampilkan halaman keranjang belanja yang berisikan daftar barang 
 * permintaan para users. 
 */
package id.ac.unsyiah.jte.mobile2014;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class KeranjangBelanjaServlet extends HttpServlet 
{
    /**
     * Tangani HTTP GET
     * Tampilkan halaman keranjang belanja
     */
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException 
    {        
        // Informasikan ke browser bahwa akan dikirim berkas html
        resp.setContentType("text/html");
        // Minta untuk bangkitkan HTML berdasarkan cetakan keranjang.jsp
        RequestDispatcher jsp = req.getRequestDispatcher("keranjang.jsp");
        // Bangkitkan HTML-nya
        jsp.forward(req, resp);        
    }
}

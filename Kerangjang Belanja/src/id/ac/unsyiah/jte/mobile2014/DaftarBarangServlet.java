/**
 * Menampilkan halaman yang berisikan daftar barang dan memproses permintaan 
 * barang oleh para users. 
 */
package id.ac.unsyiah.jte.mobile2014;

import java.io.IOException;

import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SuppressWarnings("serial")
public class DaftarBarangServlet extends HttpServlet 
{
    /**
     * Tangani HTTP GET
     * Tampilkan halaman daftar barang
     */
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException 
    {        
        // Informasikan ke browser bahwa akan dikirim berkas html
        resp.setContentType("text/html");
        // Minta untuk bangkitkan HTML berdasarkan cetakan daftar_barang.jsp
        RequestDispatcher jsp = req.getRequestDispatcher("daftar_barang.jsp");
        // Bangkitkan HTML-nya
        jsp.forward(req, resp);        
    }

    /**
     * Tangani HTTP POST
     * Proses pengirim form oleh user yang merupakan permintaan akan barang 
     * tertentu
     */
    @SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws IOException, ServletException
    {
    	// Keranjang Belanja kita
    	ArrayList<Integer> keranjangBelanja = null;
    	
    	// Ambil session yang ada. Perhatikan jangan buat baru!
		HttpSession session = req.getSession(false);
		// Jika belum ada session buat session khusus untuk keranjang belanja
		if (session == null)
			// Buat session keranjang belanja
			keranjangBelanja = buatSessionKeranjangBelanja(req);
		else
		{
			// Sessionya ada, sekarang periksa apakah session object kita ada
			keranjangBelanja = 
				(ArrayList<Integer>) session.getAttribute(NAMA_SESSION_OBJECT);
			
			if (keranjangBelanja == null)
				// Session object kita tidak ada, jadi dibuat
				keranjangBelanja = buatSessionKeranjangBelanja(req);
		}
    	
    	// Ambil informasi tombol mana yang ditekan
        String barang1 = req.getParameter("btnBarang1");
        String barang2 = req.getParameter("btnBarang2");
        String barang3 = req.getParameter("btnBarang3");
        
        if (barang1 != null) // Tombol dari barang 1 ditekan
        {
        	int jumlahBarang = keranjangBelanja.get(0) + 1;
        	keranjangBelanja.set(0, jumlahBarang);        	
        }
        else if (barang2 != null) // Tombol dari barang 2 ditekan
        {
        	int jumlahBarang = keranjangBelanja.get(1) + 1;
        	keranjangBelanja.set(1, jumlahBarang);        	
        }
        else if (barang3 != null) // Tombol dari barang 3 ditekan
        {
        	int jumlahBarang = keranjangBelanja.get(2) + 1;
        	keranjangBelanja.set(2, jumlahBarang);        	
        }

        // Update session object-nya
        session.setAttribute(NAMA_SESSION_OBJECT,  keranjangBelanja);
        
        // Kirim ke halaman kerangjang belanja
        resp.sendRedirect("/keranjang");
    }
    
    private ArrayList<Integer> buatSessionKeranjangBelanja(HttpServletRequest req)
    {
    	// Session kita akan berisikan session object yang merupakan list dari
    	// jumlah barang yang telah diminta  
    	ArrayList<Integer> keranjangBelanja = new ArrayList<Integer>();
    	// Element pertama adalah jumlah barang untuk barang 1
    	keranjangBelanja.add(0);
    	// Element kedua adalah jumlah barang untuk barang 2
    	keranjangBelanja.add(0);
    	// Element ketiga adalah jumlah barang untuk barang 3
    	keranjangBelanja.add(0);
    	
    	// Buat session, jika belum ada buat
		HttpSession session = req.getSession(true);
		// Simpan session object kita ke sessio
		session.setAttribute(NAMA_SESSION_OBJECT, keranjangBelanja);
		
		// Kembalikan keranjang belanjanya
		return keranjangBelanja; 
    }
    
    final static public String NAMA_SESSION_OBJECT = "keranjangBelanja";
}

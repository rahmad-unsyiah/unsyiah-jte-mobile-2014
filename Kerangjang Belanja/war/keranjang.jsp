<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Keranjang belanja</title>
    </head>
    <body>
        <table border="0">
            <thead>
                <tr>
                    <th colspan="2">Keranjang Belanja</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Barang 1</td>
                    <td>${sessionScope.keranjangBelanja[0]}</td>
                </tr>
                <tr>
                    <td>Barang 2</td>
                    <td>${sessionScope.keranjangBelanja[1]}</td>
                </tr>
                <tr>
                    <td>Barang 3</td>
                    <td>${sessionScope.keranjangBelanja[2]}</td>
                </tr>
            </tbody>
        </table>
        <a href="/">Daftar barang</a>
    </body>
</html>
